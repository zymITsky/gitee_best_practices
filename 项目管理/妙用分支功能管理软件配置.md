妙用分支功能管理软件配置
===================

## 引言

码云是非常棒的国产软件版本管理与托管平台，支持私有仓库、速度快，因此很多小企业和团队使用它来管理软件版本。
我们在项目开发过程中，通常需要管理三种运行环境，`开发者本地计算机`、`测试服务器`、`产品服务器`。
对应着一定有三套配置文件，其中有数据库密码等信息。配置文件是必须纳入软件版本控制的，但是合作开发经常出现配置文件被其他成员修改，
频繁的被改成对方本地的数据库密码，造成每次拉取代码都有冲突的烦恼。如何解决这个问题，对软件的版本进行有效管理呢？
这时候码云的分支管理就派上用场了。

## 准备工作

在多年的实践经验中，我建议开始一个新的项目，至少创建如下固定分支：
- master (主分支)
- test (测试服务器分支)
- production (产品服务器分支)

其中master作为我们的主分支，代表了最新的稳定的程序，但是还没有部署到生产线中;test分支是测试服务器上部署的版本;
production是产品服务器上部署的版本。

在码云上创建分支，如图所示：
![](https://gitee.com/ObjectConnect/springtest/raw/master/snapshot/1.png)
创建结果：
![](https://gitee.com/ObjectConnect/springtest/raw/master/snapshot/2.png)

除此之外，所有参加项目的小团队，在开发新的功能或者修改Bug时，都应该创建名为`团队前缀-主要目标`的分支，
例如`team1-fix-bug`，提交之后都要创建Pull-Request到master，并请求同事检查Review。

## 配置文件设置

### master分支

使用项目创建者的初始配置，不要加入.gitignore的忽略中，要包含所有的配置项。

### test分支

首先切换到test分支，根据测试服务器的实际情况，修改配置，然后提交`git commit -m "init-test-config`。

然后为test分支创建一个标签`git tag test-1.0`，记录测试服务器的第一个版本，因为将来我们可能会修改测试服务器的数据库密码等信息，也可能为系统增加新的组件，例如增加了redis。

最后提交`git push origin test`，再提交tag，`git push --tags`。

### production分支

与test分支类似，首先切换到production分支，根据产品服务器的实际情况，修改配置，然后提交`git commit -m "init-production-config`。

然后为test分支创建一个标签`git tag production-1.0`，记录生产服务器的第一个版本。

最后提交`git push origin production`，再提交tag，`git push --tags`。


## 日常部署

### 开发者本地计算机

开发者直接修改软件配置为本地的数据库密码、端口等，每次提交前要小心翼翼不要提交配置文件。每次使用`git pull`之前，
要记得使用`git stash`保存当前工作状态，当`git pull`完成之后再`git stash pop`还原自己的修改。

### 测试服务器

如果需要部署测试服务器，需要执行以下步骤：

1. 切换到test分支`git checkout test`
2. 获取最新代码`git fetch origin`
3. 将master更新合并到test`git merge origin/master`
4. 执行打包命令，如Java项目`mvn package`
5. 上传服务器部署
6. 将代码上传，方便查阅与调试`git push origin test`

如果我们只是临时想打包一个版本，甚至是测试另外一个分支`Team1-fix-bug`，不想上传到码云服务器的test分支，
我们可以在第3步执行`git merge origin/Team1-fix-bug`，并且执行完第5步之后，不要执行第6步，将修改暂时留在本地。
如果此时需要重新打包测试，可以使用`git reset --hard test-1.0`，将版本重置为以前与服务器同步的Tag，然后重新执行1~6步即可。

### 产品服务器

如果需要部署产品服务器，需要执行以下步骤：

1. 切换到test分支`git checkout production`
2. 获取最新代码`git fetch origin`
3. 将master更新合并到test`git merge origin/master`
4. 执行打包命令，如Java项目`mvn package`
5. 上传服务器部署
6. 将代码上传，方便查阅与调试`git push origin production`

## 结论

经过这样的划分，能够让工作有条不紊的进行，避免了因为配置文件错误导致的服务器部署的灾难。
非常适合小团队在创业初期阶段经常需要给客户演示，能够快速使用正确的配置文件，配置到对应的服务器上。
在快速迭代中，保持有序，而且也为使用Jenkins持续集成打下了基础。



